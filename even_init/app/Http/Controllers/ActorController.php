<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Actor;

class ActorController extends Controller
{

    public function index ()
    {
        $actors = Actor::latest('id')->select('id', 'login', 'avatar_url')->get();
        return response()->json($actors, 200);
    }

    public function store (Request $request)
    {
        if ($this->loginExists($request->login) || $this->validateActor($request) === true) {
            return response()->json(["message" => "Please provided fill all necessary form below!"], 400);
        }
    
        $response = $newActor = $this->createActor($request);
        return response()->json($response, 400);
    }

    private function validateActor($request) {

        $validator = Validator::make($request->all(), [
            'login' => ['required', 'string'],
            'avatar_url' => 'required|url'
        ]);

        return $validator->fails() ? true : false;
    }


    private function createActor ($request) 
    {
        return Actor::create([
                'login' => $request->login,
                'avatar_url' => $request->avatar_url
        ])->select('login', 'avatar_url');
    }

    /**
     * should be Handled in Exception namespace
     * @loginExists($login)
     * return true or false
     * Not Here...
     */
    private function loginExists($login)
    {
        return Actor::where('login', $login)->exists();
    }
}
