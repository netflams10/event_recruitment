<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Event;
use App\Models\Repo;

class EventController extends Controller
{
    public function index()
    {
        $events = Event::latest()->with(['actors', 'repo'])->get();
        return response()->json($events, 200);
    }
    
    public function store (Request $request, $id)
    {
        if ($this->validateEvents($request)->fails()) {
            return response()->json(['message' => "Please Complete the Evnet attributes!"], 400);
        }

        $repo = $this->saveNewRepo($request->repo_name, $request->repo_url);
        $newEvent = $this->saveNewEvent($request->event, $id, $repo);
        
        return response()->json($newEvent, 200);
    }

    /**
     * validateEvents
     * 'event', ['PushEvent', 'PullEvent']
     */
    private function validateEvents ($request)
    {
        return Validator::make($request->all(), [
            'type' => 'required',
            'repo_name' => 'required|string',
            'repo_url' => 'required|string|url',
        ]);
    }

    private function saveNewRepo ($name, $url)
    {
        return Repo::create([
            'name' => $name,
            'url' => $url
        ])->id;
    }

    private function saveNewEvent ($event, $id, $repo)
    {
        return Event::create([
            'event' => $event,
            'actor_id' => $id,
            'repo_id' => $repo
        ]);
    }
}


// 'event', ['PushEvent', 'PullEvent'],
//             'actor_id'
//             'repo_id'
