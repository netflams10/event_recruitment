<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['id', 'event', 'actor_id', 'repo_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function actor ()
    {
        return $this->hasOne(Actor::class);
    }

    public function repo ()
    {
        return $this->hasOne(Repo::class);
    }
}
