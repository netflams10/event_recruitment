<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Actor extends Model
{

    protected $fillable = ['id', 'login', 'avatar_url'];

    protected $hiddden = ['created_at', 'updated_at'];
}
