<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'App\Http\Controllers'], function () {
    Route::post('events/{id}', 'EventController@store');
    Route::post('actors', 'ActorController@store');
    Route::get('actors', 'ActorController@index');
    Route::get('events', 'EventController@index');
    
    // Route::get('uuid', function () {
    //     return Uuid::uuid3(Uuid::NAMESPACE_DNS, 'example.com')->toString();
    // });
});
